#include <string>
#include <vector>
#include <iomanip> // for arranging output in proper form , saw from stackflow.com // 

class Student{

public:
	std::string firstName;
	std::string lastName;
	int id;
	int marks1;
	float perc1;
	int marks2;
	float perc2;
	int marks3;
	float perc3;
	int mid_marks;
	float mid_perc;
	int final_marks;
	float final_perc;
	char grade;


	void set_grade(){

		float total = (perc1*0.05) + (perc2*0.10) + (perc3*0.15) + (mid_perc*0.3) + (final_perc*0.4);

		if (total < 50) grade = 'F';
		if (total > 50) grade = 'R';
		if (total > 80) grade = 'H';
	}

	void print_data(){  // unnecessay, no use in the main //
		std::cout << " NAME:\t\t " << lastName + firstName << "\n" << " ID: \t\t" << id << "\n";
		std::cout << " ASSIGNMENT-1:\t" << " marks= " << marks1 << ", percentage= " << perc1 << "\n";
		std::cout << " ASSIGNMENT-1:\t" << " marks= " << marks2 << ", percentage= " << perc2 << "\n";
		std::cout << " ASSIGNMENT-3:\t" << " marks= " << marks3 << ", percentage= " << perc3 << "\n";
		std::cout << " MIDs:\t\t" << "marks= " << mid_marks << ", percentage= " << mid_perc << "\n";
		std::cout << " ESEs:\t\t" << "marks= " << final_marks << ", percentage= " << final_perc << "\n";
		std::cout << " GRADE:\t\t" << grade << "\n\n\n";
	}

	Student(){                //Consructor//
		firstName=" ";
		lastName=" ";
		id= 0;
		marks1=0;
		perc1=0.0;
		marks2=0;
		perc2=0.0;
		marks3=0;
		perc3=0.0;
		mid_marks=0;
		mid_perc=0.0;
		final_marks=0;
		final_perc=0.0;
		grade=' ';
	}

};

