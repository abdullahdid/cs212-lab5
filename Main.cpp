#include <iostream>
#include "Header.h"
#include <fstream>
#include <sstream>
#include <iomanip> // for arranging output in proper form , saw from stackflow.com // 

using namespace std;

int main() 
{

	ifstream inFile("sample-result-sheet.csv");

	if (!inFile.is_open()){
		cerr << "File is not opened" << endl;
	}

	string line;
	vector<string> data;

	int no_of_lines=0;                               // no of lines in csv file //
	while (getline(inFile, line)){          // no need for eof , when getline reaches the end i-e it can't read anything , it returns 0 value itself and loop will end //	
		data.push_back(line);
		no_of_lines++;                    // no of lines in csv file will give us the number of students //
	}



	cout << setw(12) << "NAMES" << setw(19) << "ID" << setw(15) <<"GRADES\n";   // setw() is a function of "iomanip" library inlcuded above , it will arrange output //
	cout << "----------------------------------------------------\n";
	cout << "----------------------------------------------------\n";
	


	Student students[50]; // making instances of all the students in the csv file using array //

	for (int i = 0; i < (no_of_lines) ; i++){ // "i" will be the line number and also give a student from the array //

		string str = data[i];

		string field;
		istringstream customStream(str);

		vector <string> singleStudRecord;

		while (getline(customStream, field, ',')){ // default ender of getline was "\n" like above but now we have made it comma ourself //

			singleStudRecord.push_back(field);

		}

		// index of percentages are 4,6,8,10,12//
		// index of marks are 3,5,7,9,11, //
		
		students[i].lastName = singleStudRecord[0];
		students[i].firstName = singleStudRecord[1];
		students[i].id = stof(singleStudRecord[2]);
		students[i].marks1 = stoi(singleStudRecord[3]);
		students[i].marks2 = stoi(singleStudRecord[5]);
		students[i].marks3 = stoi(singleStudRecord[7]);
		students[i].mid_marks = stoi(singleStudRecord[9]);
		students[i].final_marks = stoi(singleStudRecord[11]);
		
		students[i].perc1 = stof(singleStudRecord[4]);
		students[i].perc2 = stof(singleStudRecord[6]);
		students[i].perc3 = stof(singleStudRecord[8]);
		students[i].mid_perc = stof(singleStudRecord[10]);
		students[i].final_perc = stof(singleStudRecord[12]);
		
		students[i].set_grade();
		
		cout << setw(20) << students[i].lastName + students[i].firstName << setw(12) << students[i].id << setw(12) << students[i].grade << "\n";
		cout << endl;

	}



	cin.ignore(); // equivalent to _getch() //
	return 0;


}


